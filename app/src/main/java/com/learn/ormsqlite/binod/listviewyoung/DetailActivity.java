package com.learn.ormsqlite.binod.listviewyoung;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {
    private static final String IMAGE_URL_BASE="";
    private TextView Title;
    private TextView Body;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        String coverID =
                this.getIntent().getExtras().getString("coverID");
        Title=(TextView)findViewById(R.id.title);
        Body=(TextView)findViewById(R.id.body);
        Title.setText(coverID);
    }
}
