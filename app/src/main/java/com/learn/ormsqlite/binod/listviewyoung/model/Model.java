package com.learn.ormsqlite.binod.listviewyoung.model;

import java.io.Serializable;

/**
 * Created by binod on 8/7/2017.
 */

public class Model implements Serializable {
    private static final long serialVersionUID = -8959832007991513854L;
    private String title,comments;

    public Model(){
    }

    public Model(String title, String comments) {
        this.title = title;
        this.comments = comments;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
