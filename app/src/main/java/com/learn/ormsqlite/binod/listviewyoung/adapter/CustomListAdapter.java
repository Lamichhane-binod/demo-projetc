package com.learn.ormsqlite.binod.listviewyoung.adapter;

import com.learn.ormsqlite.binod.listviewyoung.R;
import com.learn.ormsqlite.binod.listviewyoung.model.Model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.learn.ormsqlite.binod.listviewyoung.R;
import com.learn.ormsqlite.binod.listviewyoung.model.Model;

import java.util.List;

/**
 * Created by binod on 8/7/2017.
 */

public class CustomListAdapter extends BaseAdapter{
    private Activity activity;
    private LayoutInflater inflater;
    private List<Model> modelItems;

    public CustomListAdapter(Activity activity, List<Model> modelItems) {
        this.activity = activity;
        this.modelItems = modelItems;
    }

    @Override
    public int getCount() {
        return modelItems.size();
    }

    @Override
    public Object getItem(int location) {
        return modelItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater==null)
            inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView==null)
            convertView=inflater.inflate(R.layout.list_row,null);

        TextView title=(TextView)convertView.findViewById(R.id.title);
        TextView comment=(TextView)convertView.findViewById(R.id.comments);

        Model m=modelItems.get(position);

        title.setText(m.getTitle());

        comment.setText(m.getComments());
        return convertView;
    }
}
